@extends('master')

@section('content')
<div id="primary" class="content-area" style="width:100%;">		
    <div class="container-fluid">
        <div class="row fondo-servicio polarizados">
            <div class="col-xs-12">
                &nbsp;
            </div>
        </div>
        <div class="row fondo-azul">				
            <div class="col-xs-12" style="padding:35px;">
                <h1 class="text-blanco text-titulo">
                    Polarizados
                </h1>
                <span style="color: #ffffff;">Venta e instalación de películas de seguridad y polarizados para todo tipo de vehículos, oficinas, apartamentos y casas. <br>Manejamos papel americano garantizado.</span>
            </div>
        </div>
        <div class="row">				
            <div class="col-xs-12 col-md-7">
                <div class="row">
                    <div class="col-xs-6 col-sm-4">
                        <a href="{{ asset('/img/servicios/autoforros_polarizado_4.jpg') }}" data-toggle="lightbox" data-gallery="multiimages" data-title="">
                            <img class="img-responsive" style="width:100%;margin-top:10px;" src="{{ asset('/img/thumbs/autoforros_polarizado_4.jpg') }}" />
                        </a>
                    </div>
                    <div class="col-xs-6 col-sm-4">
                        <a href="{{ asset('/img/servicios/autoforros_polarizado_5.jpg') }}" data-toggle="lightbox" data-gallery="multiimages" data-title="">
                            <img class="img-responsive" style="width:100%;margin-top:10px;" src="{{ asset('/img/thumbs/autoforros_polarizado_5.jpg') }}" />
                        </a>
                    </div>
                    <div class="col-xs-6 col-sm-4">
                        <a href="{{ asset('/img/servicios/autoforros_polarizado_6.jpg') }}" data-toggle="lightbox" data-gallery="multiimages" data-title="">
                            <img class="img-responsive" style="width:100%;margin-top:10px;" src="{{ asset('/img/thumbs/autoforros_polarizado_6.jpg') }}" />
                        </a>
                    </div>
                    <div class="col-xs-6 col-sm-4">
                        <a href="{{ asset('/img/servicios/autoforros_polarizado_7.jpg') }}" data-toggle="lightbox" data-gallery="multiimages" data-title="">
                            <img class="img-responsive" style="width:100%;margin-top:10px;" src="{{ asset('/img/thumbs/autoforros_polarizado_7.jpg') }}" />
                        </a>
                    </div>
                    <div class="col-xs-6 col-sm-4">
                        <a href="{{ asset('/img/servicios/autoforros_polarizado_8.jpg') }}" data-toggle="lightbox" data-gallery="multiimages" data-title="">
                            <img class="img-responsive" style="width:100%;margin-top:10px;" src="{{ asset('/img/thumbs/autoforros_polarizado_8.jpg') }}" />
                        </a>
                    </div>
                    <div class="col-xs-6 col-sm-4">
                        <a href="{{ asset('/img/servicios/autoforros_polarizado_9.jpg') }}" data-toggle="lightbox" data-gallery="multiimages" data-title="">
                            <img class="img-responsive" style="width:100%;margin-top:10px;" src="{{ asset('/img/thumbs/autoforros_polarizado_9.jpg') }}" />
                        </a>
                    </div>
                    <div class="col-xs-6 col-sm-4">
                        <a href="{{ asset('/img/servicios/autoforros_polarizado_10.jpg') }}" data-toggle="lightbox" data-gallery="multiimages" data-title="">
                            <img class="img-responsive" style="width:100%;margin-top:10px;" src="{{ asset('/img/thumbs/autoforros_polarizado_10.jpg') }}" />
                        </a>
                    </div>
                    <div class="col-xs-6 col-sm-4">
                        <a href="{{ asset('/img/servicios/autoforros_polarizado_11.jpg') }}" data-toggle="lightbox" data-gallery="multiimages" data-title="">
                            <img class="img-responsive" style="width:100%;margin-top:10px;" src="{{ asset('/img/thumbs/autoforros_polarizado_11.jpg') }}" />
                        </a>
                    </div>
                    <div class="col-xs-6 col-sm-4">
                        <a href="{{ asset('/img/servicios/autoforros_polarizado_12.jpg') }}" data-toggle="lightbox" data-gallery="multiimages" data-title="">
                            <img class="img-responsive" style="width:100%;margin-top:10px;" src="{{ asset('/img/thumbs/autoforros_polarizado_12.jpg') }}" />
                        </a>
                    </div>
                </div>
            </div>
            @include('form_cotizacion')
        </div>
    </div>		
</div><!-- .content-area -->

@stop