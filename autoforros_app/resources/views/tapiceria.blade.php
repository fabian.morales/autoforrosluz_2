@extends('master')

@section('content')
<div id="primary" class="content-area" style="width:100%;">		
    <div class="container-fluid">
        <div class="row fondo-servicio tapiceria">
            <div class="col-xs-12">
                &nbsp;
            </div>
        </div>
        <div class="row fondo-azul">				
            <div class="col-xs-12" style="padding:35px;">
                <h1 class="text-blanco text-titulo">
                    Tapicería
                </h1>
                <p style="text-align: justify;"><span style="color: #ffffff;">Contamos con el servicio de tapicería para todo tipo de vehículos, ofreciendo de manera amplia nuestra experiencia en tapicería para:</span></p>

                <div class="container">
                    <div class="col-md-3">
                    <ul style="list-style: none;">
                        <li><span style="color: #ffffff;"><i class="fa fa-check-circle"></i> Techos</span></li>
                        <li><span style="color: #ffffff;"><i class="fa fa-check-circle"></i> Cartera</span></li>
                        <li><span style="color: #ffffff;"><i class="fa fa-check-circle"></i> Cojineria</span></li>
                        <li><span style="color: #ffffff;"><i class="fa fa-check-circle"></i> Pisos</span></li>
                        <li><span style="color: #ffffff;"><i class="fa fa-check-circle"></i> Bandejas</span></li>
                    </ul>
                    </div>
                    <div class="col-md-4">
                        <ul style="list-style: none;">
                            <li><span style="color: #ffffff;"><i class="fa fa-check-circle"></i> Parales</span></li>
                            <li><span style="color: #ffffff;"><i class="fa fa-check-circle"></i> Parasoles</span></li>
                            <li><span style="color: #ffffff;"><i class="fa fa-check-circle"></i> Forros palanca de cambio y emergencia</span></li>
                            <li><span style="color: #ffffff;"><i class="fa fa-check-circle"></i> Cinturones de seguridad</span></li>
                            <li><span style="color: #ffffff;"><i class="fa fa-check-circle"></i> Pijamas sobre medidas</span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">				
            <div class="col-xs-12 col-md-7">
                <div class="row">
                    <div class="col-xs-6 col-sm-4">
                        <a href="{{ asset('/img/servicios/autoforros_tapiceria_1.jpg') }}" data-toggle="lightbox" data-gallery="multiimages" data-title="">
                            <img class="img-responsive" style="width:100%;margin-top:10px;" src="{{ asset('/img/thumbs/autoforros_tapiceria_1.jpg') }}" />
                        </a>
                    </div>
                    <div class="col-xs-6 col-sm-4">
                        <a href="{{ asset('/img/servicios/autoforros_tapiceria_2.jpg') }}" data-toggle="lightbox" data-gallery="multiimages" data-title="">
                            <img class="img-responsive" style="width:100%;margin-top:10px;" src="{{ asset('/img/thumbs/autoforros_tapiceria_2.jpg') }}" />
                        </a>
                    </div>
                    <div class="col-xs-6 col-sm-4">
                        <a href="{{ asset('/img/servicios/autoforros_tapiceria_3.jpg') }}" data-toggle="lightbox" data-gallery="multiimages" data-title="">
                            <img class="img-responsive" style="width:100%;margin-top:10px;" src="{{ asset('/img/thumbs/autoforros_tapiceria_3.jpg') }}" />
                        </a>
                    </div>
                    <div class="col-xs-6 col-sm-4">
                        <a href="{{ asset('/img/servicios/autoforros_tapiceria_4.jpg') }}" data-toggle="lightbox" data-gallery="multiimages" data-title="">
                            <img class="img-responsive" style="width:100%;margin-top:10px;" src="{{ asset('/img/thumbs/autoforros_tapiceria_4.jpg') }}" />
                        </a>
                    </div>
                    <div class="col-xs-6 col-sm-4">
                        <a href="{{ asset('/img/servicios/autoforros_tapiceria_5.jpg') }}" data-toggle="lightbox" data-gallery="multiimages" data-title="">
                            <img class="img-responsive" style="width:100%;margin-top:10px;" src="{{ asset('/img/thumbs/autoforros_tapiceria_5.jpg') }}" />
                        </a>
                    </div>
                    <div class="col-xs-6 col-sm-4">
                        <a href="{{ asset('/img/servicios/autoforros_tapiceria_6.jpg') }}" data-toggle="lightbox" data-gallery="multiimages" data-title="">
                            <img class="img-responsive" style="width:100%;margin-top:10px;" src="{{ asset('/img/thumbs/autoforros_tapiceria_6.jpg') }}" />
                        </a>
                    </div>
                    <div class="col-xs-6 col-sm-4">
                        <a href="{{ asset('/img/servicios/autoforros_tapiceria_7.jpg') }}" data-toggle="lightbox" data-gallery="multiimages" data-title="">
                            <img class="img-responsive" style="width:100%;margin-top:10px;" src="{{ asset('/img/thumbs/autoforros_tapiceria_7.jpg') }}" />
                        </a>
                    </div>
                    <div class="col-xs-6 col-sm-4">
                        <a href="{{ asset('/img/servicios/autoforros_tapiceria_8-1.jpg') }}" data-toggle="lightbox" data-gallery="multiimages" data-title="">
                            <img class="img-responsive" style="width:100%;margin-top:10px;" src="{{ asset('/img/thumbs/autoforros_tapiceria_8-1.jpg') }}" />
                        </a>
                    </div>
                    <div class="col-xs-6 col-sm-4">
                        <a href="{{ asset('/img/servicios/autoforros_tapiceria_9.jpg') }}" data-toggle="lightbox" data-gallery="multiimages" data-title="">
                            <img class="img-responsive" style="width:100%;margin-top:10px;" src="{{ asset('/img/thumbs/autoforros_tapiceria_9.jpg') }}" />
                        </a>
                    </div>
                    <div class="col-xs-6 col-sm-4">
                        <a href="{{ asset('/img/servicios/autoforros_tapiceria_11.jpg') }}" data-toggle="lightbox" data-gallery="multiimages" data-title="">
                            <img class="img-responsive" style="width:100%;margin-top:10px;" src="{{ asset('/img/thumbs/autoforros_tapiceria_11.jpg') }}" />
                        </a>
                    </div>
                    <div class="col-xs-6 col-sm-4">
                        <a href="{{ asset('/img/servicios/autoforros_tapiceria_12.jpg') }}" data-toggle="lightbox" data-gallery="multiimages" data-title="">
                            <img class="img-responsive" style="width:100%;margin-top:10px;" src="{{ asset('/img/thumbs/autoforros_tapiceria_12.jpg') }}" />
                        </a>
                    </div>
                </div>
            </div>
            @include('form_cotizacion')
        </div>
    </div>		
</div><!-- .content-area -->

@stop