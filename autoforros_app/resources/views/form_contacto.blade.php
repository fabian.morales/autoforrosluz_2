<div class="col-xs-12 col-md-6">
    <script src="https://cdn.jotfor.ms/static/prototype.forms.js" type="text/javascript"></script>
    <script src="https://cdn.jotfor.ms/static/jotform.forms.js?3.3.14439" type="text/javascript"></script>
    <script type="text/javascript"> JotForm.init(function(){ JotForm.alterTexts({"alphabetic":"Este campo solo puede contener letras","alphanumeric":"Este campo solo puede contener letras y números.","ccInvalidCVC":"El número CVC no es válido.","ccInvalidExpireDate":"La fecha de expiración no es válida","ccInvalidNumber":"su número de terjeta de crédito no es válido","ccMissingDetails":"Por favor rellene los datos de su Tarjeta de Crédito","ccMissingDonation":"Ingresa por favor una cantidad a donar","ccMissingProduct":"Por favor seleccione al menos un producto.","characterLimitError":"Too many Characters. The limit is","characterMinLimitError":"Too few characters. The minimum is","confirmClearForm":"¿Está seguro de querer borra el formulario?","confirmEmail":"Correo electrónico no coincide","currency":"Este campo sólo puede contener valores de moneda","cyrillic":"Este campo solo puede contener caracteres cirílicos","dateInvalid":"Esta fecha no es valida. El formato de fecha es {format}","dateInvalidSeparate":"This date is not valid. Enter a valid {element}.","dateLimited":"Esta fecha no se encuentra disponible.","disallowDecimals":"Por favor, introduzca un número entero.","email":"Introduzca una dirección e-mail válida","fillMask":"El valor de campo debe llenar la mascara","freeEmailError":"Cuentas de correos gratis no se permiten","generalError":"Existen errores en el formulario, por favor corríjalos antes de continuar.","generalPageError":"Hay errores en esta página. Por favor, corríjalos antes de continuar.","gradingScoreError":"El puntaje total debería ser sólo \"menos que\" o \"igual que\"","incompleteFields":"Existen campos requeridos incompletos. Por favor complételos.","inputCarretErrorA":"El valor introducido no puede ser menor que el mínimo especificado:","inputCarretErrorB":"Entrada no debe ser mas grande que el valor maximo:","lessThan":"Tu cuenta debería ser menor o igual que","maxDigitsError":"El máximo de dígitos permitido es","maxSelectionsError":"El número máximo de selecciones es","minSelectionsError":"El número mínimo de selección requerido es","multipleFileUploads_emptyError":"El fichero {file} está vacío; por favor, selecciona de nuevo los ficheros sin él.","multipleFileUploads_fileLimitError":"Solo {fileLimit} carga de archivos permitida.","multipleFileUploads_minSizeError":"{file} is demasiado pequeño, el tamaño mínimo de fichero es: {minSizeLimit}.","multipleFileUploads_onLeave":"Se están cargando los ficheros, si cierras ahora, se cancelará dicha carga.","multipleFileUploads_sizeError":"{file} es demasiado grande; el tamaño del archivo no debe superar los {sizeLimit}.","multipleFileUploads_typeError":"El fichero {file} posee una extensión no permitida. Sólo están permitidas las extensiones {extensions}.","numeric":"Este campo sólo admite valores numéricos","pastDatesDisallowed":"La fecha debe ser futura","pleaseWait":"Por favor, espere ...","required":"Campo requerido.","requireEveryCell":"Todas las celdas son requeridas.","requireEveryRow":"Todas las filas son obligatorias.","requireOne":"Por lo menos un campo requerido","submissionLimit":"¡Lo siento! Sólo se permite una entrada. Múltiples envíos están desactivados para este formulario.","uploadExtensions":"Solo puede subir los siguientes archivos:","uploadFilesize":"Tamaño del archivo no puede ser mayor que:","uploadFilesizemin":"Tamañao de archivo no puede ser menos de:","url":"Este campo solo contiene una URL v&Atilde;&iexcl;lida","wordLimitError":"Too many words. The limit is","wordMinLimitError":"Too few words. The minimum is"}); JotForm.clearFieldOnHide="disable"; JotForm.onSubmissionError="jumpToFirstError"; });
    </script>
    <form class="jotform-form" action="https://submit.jotformz.com/submit/62223334271648/" method="post" name="form_62223334271648" id="62223334271648" accept-charset="utf-8">
        <input type="hidden" name="formID" value="62223334271648" />
        <div class="form-all">
            <ul class="form-section page-section row" style="list-style: none">
                <li class="form-line jf-required form-group col-xs-12" data-type="control_textbox" id="id_4">
                    <label class="form-label form-label-left form-label-auto" id="label_4" for="input_4"> Nombre Completo: <span class="form-required"> * </span></label>
                    <div id="cid_4" class="form-input jf-required">
                        <input type="text" class=" form-textbox validate[required]" data-type="input-textbox" id="input_4" name="q4_nombreCompleto4" size="20" value="" />
                    </div>
                </li>
                <li class="form-line jf-required form-group col-xs-12" data-type="control_email" id="id_3">
                    <label class="form-label form-label-left form-label-auto" id="label_3" for="input_3"> Email: <span class="form-required"> * </span></label>
                    <div id="cid_3" class="form-input jf-required">
                        <input type="email" class=" form-textbox validate[required, Email]" id="input_3" name="q3_email" size="20" value="" />
                    </div>
                </li>
                <li class="form-line jf-required form-group col-xs-12" data-type="control_textbox" id="id_5">
                    <label class="form-label form-label-left form-label-auto" id="label_5" for="input_5"> Celular o Móvil: <span class="form-required"> * </span></label>
                    <div id="cid_5" class="form-input jf-required">
                        <input type="text" class=" form-textbox validate[required]" data-type="input-textbox" id="input_5" name="q5_celularO" size="20" value="" />
                    </div>
                </li>
                <li class="form-line jf-required form-group col-xs-12" data-type="control_textarea" id="id_6">
                    <label class="form-label form-label-left form-label-auto" id="label_6" for="input_6"> ¿En qué podemos ayudarlo? <span class="form-required"> * </span></label>
                    <div id="cid_6" class="form-input jf-required">
                        <textarea id="input_6" class="form-textarea validate[required]" name="q6_enQue" cols="40" rows="6"></textarea>
                    </div>
                </li>
                <li class="form-line col-xs-12 form-group" data-type="control_button" id="id_7">
                    <div id="cid_7" class="form-input-wide">
                        <div style="text-align:center" class="form-buttons-wrapper">
                            <button id="input_7" type="submit" class="form-submit-button btn btn-primary"> Enviar Información </button>
                        </div>
                    </div>
                </li>
                <li style="display:none" class="col-xs-12 form-group"> Should be Empty: <input type="text" name="website" value="" /> </li>
            </ul>
        </div>
        <input type="hidden" id="simple_spc" name="simple_spc" value="62223334271648" />
        <script type="text/javascript"> document.getElementById("si" + "mple" + "_spc").value = "62223334271648-62223334271648";</script>
    </form>
    <script type="text/javascript">JotForm.ownerView=true;</script>
</div>
<div class="col-xs-12 col-md-6">
    <p style="margin-bottom:0px; font-size:20px;">Dirección:</p>
    Calle 52 Cra 1D Esquina. Centro Comercial Carrera. <br>Local 121<br>
    <p style="margin-bottom:0px; font-size:20px;">Teléfono:</p>
    (57 - 2) 449 03 39<br>
    <p style="margin-bottom:0px; font-size:20px;">Celular:</p>
    (57) 312 394 53 55<br>
    <p style="margin-bottom:0px; font-size:20px;">Horario de Atención</p>
    Lunes a Viernes de 8:00AM a 5:30PM<br>
    Sábados de 8:00AM a 3:00PM
    
    <p>Su opinión es muy importante para nosotros. Cuéntenos aquí <a href="https://plus.google.com/+Autoforrosluz/about?gmbpt=true&hl=es-419&_ga=1.73794642.1302996556.1471539198" target="_blank">¿Cómo le pareció el servicio prestado por Autoforros Luz?</a></p>
</div>