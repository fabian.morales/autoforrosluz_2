@extends('master')

@section('content')
<div id="primary" class="content-area" style="width:100%;">		
    <div class="container-fluid">
        <div class="row fondo-servicio aire_acondicionado">
            <div class="col-xs-12">
                &nbsp;
            </div>
        </div>
        <div class="row fondo-azul">				
            <div class="col-xs-12" style="padding:35px;">
                <h1 class="text-blanco text-titulo">
                    Aire acondicionado
                </h1>
                
                <p style="text-align: justify;"><span style="color: #ffffff;">Ofrecemos el servicio de instalación, mantenimiento preventivo y correctivo del aire acondicionado de su vehículo, realizando el diagnostico y evaluación sin costo alguno, permitiéndonos asesorar al cliente en todo el proceso y garantizando un excelente trabajo mediante la utilización de buenas practicas y trabajando con los mejores repuestos y herramientas del mercado.</span></p>

                <ul style="list-style:none;">
                    <li><span style="color: #ffffff;"><i class="fa fa-check-circle" aria-hidden="true"></i> Diagnostico sin ningún costo.</span></li>
                    <li><span style="color: #ffffff;"><i class="fa fa-check-circle" aria-hidden="true"></i> Atención personalizada.</span></li>
                    <li><span style="color: #ffffff;"><i class="fa fa-check-circle" aria-hidden="true"></i> Servicio garantizado, ágil y rápido.</span></li>
                    <li><span style="color: #ffffff;"><i class="fa fa-check-circle" aria-hidden="true"></i> Manejamos repuestos para todas las marcas y modelos.</span></li>
                    <li><span style="color: #ffffff;"><i class="fa fa-check-circle" aria-hidden="true"></i> Precios competitivos en el mercado.</span></li>
                </ul>
            </div>
        </div>
        <div class="row">				
            <div class="col-xs-12 col-md-7">
                <div class="row">
                    <div class="col-xs-6 col-sm-4">
                        <a href="{{ asset('/img/servicios/autoforros_aire_1.jpg') }}" data-toggle="lightbox" data-gallery="multiimages" data-title="">
                            <img class="img-responsive" style="width:100%;margin-top:10px;" src="{{ asset('/img/thumbs/autoforros_aire_1.jpg') }}" />
                        </a>
                    </div>
                    <div class="col-xs-6 col-sm-4">
                        <a href="{{ asset('/img/servicios/autoforros_aire_2.jpg') }}" data-toggle="lightbox" data-gallery="multiimages" data-title="">
                            <img class="img-responsive" style="width:100%;margin-top:10px;" src="{{ asset('/img/thumbs/autoforros_aire_2.jpg') }}" />
                        </a>
                    </div>
                    <div class="col-xs-6 col-sm-4">
                        <a href="{{ asset('/img/servicios/autoforros_aire_3.jpg') }}" data-toggle="lightbox" data-gallery="multiimages" data-title="">
                            <img class="img-responsive" style="width:100%;margin-top:10px;" src="{{ asset('/img/thumbs/autoforros_aire_3.jpg') }}" />
                        </a>
                    </div>
                    <div class="col-xs-6 col-sm-4">
                        <a href="{{ asset('/img/servicios/autoforros_aire_4.jpg') }}" data-toggle="lightbox" data-gallery="multiimages" data-title="">
                            <img class="img-responsive" style="width:100%;margin-top:10px;" src="{{ asset('/img/thumbs/autoforros_aire_4.jpg') }}" />
                        </a>
                    </div>
                    <div class="col-xs-6 col-sm-4">
                        <a href="{{ asset('/img/servicios/autoforros_aire_5.jpg') }}" data-toggle="lightbox" data-gallery="multiimages" data-title="">
                            <img class="img-responsive" style="width:100%;margin-top:10px;" src="{{ asset('/img/thumbs/autoforros_aire_5.jpg') }}" />
                        </a>
                    </div>
                    <div class="col-xs-6 col-sm-4">
                        <a href="{{ asset('/img/servicios/autoforros_aire_6.jpg') }}" data-toggle="lightbox" data-gallery="multiimages" data-title="">
                            <img class="img-responsive" style="width:100%;margin-top:10px;" src="{{ asset('/img/thumbs/autoforros_aire_6.jpg') }}" />
                        </a>
                    </div>
                </div>
            </div>
            @include('form_cotizacion')
        </div>
    </div>		
</div><!-- .content-area -->

@stop