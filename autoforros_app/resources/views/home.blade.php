@extends('master')

@section('content')
<div id="primary" class="content-area" style="width:100%;">
    <div class="row">
        <div class="col-xs-12">
            <ul id="slider-home">
                <li><img src="{{ asset('/img/slider.png') }}" /></li>
                <li><img src="{{ asset('/img/slider.png') }}" /></li>
                <li><img src="{{ asset('/img/slider.png') }}" /></li>
            </ul>
        </div>
    </div>
    <div class="container-fluid" style="padding:20px;">
        <h1 class="text-center text-azul text-titulo">NUESTROS SERVICIOS</h1>
    </div>
    <div class="container-fluid">
        <div class="row servicios-home">
            <div class="col-xs-6 col-md-3">
                <div class="col-xs-12 fondo-azul" style="min-height:220px;"><br>
                    <div class="block-img-servicios">
                        <div class="hidden-lg hidden-md">								
                            <br />
                            <br />
                            <br />
                        </div>
                        <img src="{{ asset('/img/servicio-revision.png') }}" class="img-responsive center-block" />
                    </div>
                    <h5 class="text-center text-blanco text-servicios">REVISI&Oacute;N Y MANTENIMIENTO<br /><b>DE AIRE ACONDICIONADO</b></h5><br />
                    <a href="/aire-acondicionado"><button class="btn btn-default center-block">Ver m&aacute;s</button></a><br />
                </div>
            </div>
            <div class="col-xs-6 col-md-3">
                <div class="col-xs-12 fondo-azul" style="min-height:220px;"><br>
                    <div class="block-img-servicios">
                        <div class="hidden-lg hidden-md">								
                            <br>
                            <br>								
                        </div>
                        <img src="{{ asset('/img/servicio-sonido.png') }}" class="img-responsive center-block">
                    </div>
                    <h5 class="text-center text-blanco text-servicios">VENTA E INSTALACI&Oacute;N DE<br><b>POLARIZADOS Y PELICULAS DE SEGURIDAD</b></h5><br />
                    <a href="/polarizados"><button class="btn btn-default center-block">Ver m&aacute;s</button></a><br />
                </div>
            </div>
            <div class="col-xs-6 col-md-3">
                <div class="col-xs-12 fondo-azul" style="min-height:220px;"><br />
                    <div class="block-img-servicios">							
                        <img src="{{ asset('/img/servicio-alarma.png') }}" class="img-responsive center-block" />
                    </div>
                    <h5 class="text-center text-blanco text-servicios">SERVICIO DE TAPICERIA<br /><b>EN GENERAL</b></h5><br />
                    <a href="/tapiceria"><button class="btn btn-default center-block">Ver m&aacute;s</button></a><br />
                </div>
            </div>
            <div class="col-xs-6 col-md-3">
                <div class="col-xs-12 fondo-azul" style="min-height:220px;"><br>
                    <div class="block-img-servicios">
                        <img src="{{ asset('/img/servicio-forros.png')  }}" class="img-responsive center-block" />
                    </div>
                    <h5 class="text-center text-blanco text-servicios">FABRICACI&Oacute;N DE FORROS<br><b>PARA VEH&Iacute;CULOS A LA MEDIDA</b></h5><br />
                    <a href="/forros"><button class="btn btn-default center-block">Ver m&aacute;s</button></a><br />
                </div>
            </div>
        </div>
    </div>		
</div><!-- .content-area -->
@stop

@section('js_body')
<script type="text/javascript">
    (function($){
        $(document).ready(function() {
            $('#slider-home').lightSlider({
                item: 1,
                enableTouch:true,
                enableDrag:true,
                freeMove:true,
                loop: true,
                pager: false
            });
        });
    })(jQuery);
</script>
@stop