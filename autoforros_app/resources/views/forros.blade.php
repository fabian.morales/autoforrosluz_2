@extends('master')

@section('content')
<div id="primary" class="content-area" style="width:100%;">		
    <div class="container-fluid">
        <div class="row fondo-servicio forros">
            <div class="col-xs-12">
                &nbsp;
            </div>
        </div>
        <div class="row fondo-azul">				
            <div class="col-xs-12" style="padding:35px;">
                <h1 class="text-blanco text-titulo">
                    Forros
                </h1>
                <p style="margin-top:0px;margin-bottom:0px;color:#fff;">
                    <p style="text-align: justify;"><span style="color: #ffffff;">Fabricamos y personalizamos forros garantizados con las medidas originales para todas las marcas de vehículos proporcionando protección para la cojineria y a su vez dando elegancia y confort.</span>
                    <span style="color: #ffffff;">Manejamos cuero, vinilcuero, variedad de precios en telas, estilos y colores.</span></p>
                </p>
            </div>
        </div>
        <div class="row">				
            <div class="col-xs-12 col-md-7">
                <div class="row">
                    <div class="col-xs-6 col-sm-4">
                        <a href="{{ asset('/img/servicios/autoforros_forros_1.jpg') }}" data-toggle="lightbox" data-gallery="multiimages" data-title="">
                            <img class="img-responsive" style="width:100%;margin-top:10px;" src="{{ asset('/img/thumbs/autoforros_forros_1.jpg') }}" />
                        </a>
                    </div>
                    <div class="col-xs-6 col-sm-4">
                        <a href="{{ asset('/img/servicios/autoforros_forros_2.jpg') }}" data-toggle="lightbox" data-gallery="multiimages" data-title="">
                            <img class="img-responsive" style="width:100%;margin-top:10px;" src="{{ asset('/img/thumbs/autoforros_forros_2.jpg') }}" />
                        </a>
                    </div>
                    <div class="col-xs-6 col-sm-4">
                        <a href="{{ asset('/img/servicios/autoforros_forros_3.jpg') }}" data-toggle="lightbox" data-gallery="multiimages" data-title="">
                            <img class="img-responsive" style="width:100%;margin-top:10px;" src="{{ asset('/img/thumbs/autoforros_forros_3.jpg') }}" />
                        </a>
                    </div>
                    <div class="col-xs-6 col-sm-4">
                        <a href="{{ asset('/img/servicios/autoforros_forros_4.jpg') }}" data-toggle="lightbox" data-gallery="multiimages" data-title="">
                            <img class="img-responsive" style="width:100%;margin-top:10px;" src="{{ asset('/img/thumbs/autoforros_forros_4.jpg') }}" />
                        </a>
                    </div>
                    <div class="col-xs-6 col-sm-4">
                        <a href="{{ asset('/img/servicios/autoforros_forros_5.jpg') }}" data-toggle="lightbox" data-gallery="multiimages" data-title="">
                            <img class="img-responsive" style="width:100%;margin-top:10px;" src="{{ asset('/img/thumbs/autoforros_forros_5.jpg') }}" />
                        </a>
                    </div>
                    <div class="col-xs-6 col-sm-4">
                        <a href="{{ asset('/img/servicios/autoforros_forros_6.jpg') }}" data-toggle="lightbox" data-gallery="multiimages" data-title="">
                            <img class="img-responsive" style="width:100%;margin-top:10px;" src="{{ asset('/img/thumbs/autoforros_forros_6.jpg') }}" />
                        </a>
                    </div>
                </div>
            </div>
            @include('form_cotizacion')
        </div>
    </div>		
</div><!-- .content-area -->

@stop