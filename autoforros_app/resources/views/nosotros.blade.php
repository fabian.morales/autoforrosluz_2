@extends('master')

@section('content')
<div id="primary" class="content-area" style="width:100%;">		
    <div class="container-fluid">
        <div class="row fondo-servicio nosotros">
            <div class="col-xs-12">
                &nbsp;
            </div>
        </div>
        <div class="row fondo-azul">
            <div class="col-xs-12" style="padding:40px;">
                <h1 class="text-blanco text-titulo">QUI&Eacute;NES SOMOS</h1>
                <br />
                <p class="text-blanco">Somos una empresa con una trayectoria de 14 años en el mercado de lujos y accesorios para el sector automotriz, 
                    especializada en venta y mantenimiento de aire acondicionado, fabricacion de forros y tapiceria en general, laboratorio de radio 
                    comunicaciones, calcomanias, alarma y bloqueo, polarizados,taximetros,sonido y fabricación de toda clase de empotres.
                    Contamos con la experiencia para el embellecimiento y confort de toda clase de vehiculos. En AUTOFORROS LUZ tenemos un grupo de 
                    trabajo especializado en cada área de servicio para brindar satisfación a cada una de las necesidades de nuestros clientes.</p>
            </div>
        </div>
        <br />
        <div class="row fondo-azul">				
            <div class="col-xs-12" style="padding:40px;">
                <h2 class="text-blanco text-titulo">MISIÓN</h2>
                <br />
                <p class="text-blanco">Nuestra empresa esta dedicada a la venta e instalación de lujos y accesorios para toda clase de vehículos. 
                    Comprometida en satisfacer las necesidades de sus clientes, ofreciendo productos de alta calidad, trabajando con responsabilidad, 
                    eficiencia y creatividad, consolidando una orientación adecuada en los procesos de compra.</p>
                
                <h2 class="text-blanco text-titulo">VISIÓN</h2>
                <br />
                <p class="text-blanco">Ser una empresa lider a nivel nacional, reconocida por sus altos estandares de calidad en sus productos y servicios, 
                    teniendo apertura de nuevas sucursales y amplios establecimientos para brindar mayor comodidad y un mejor servicio a nuestros clientes.</p>
            </div>
        </div>
    </div>		
</div><!-- .content-area -->

@stop