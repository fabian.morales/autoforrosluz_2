@extends('master')

@section('content')
<div id="primary" class="content-area" style="width:100%;">		
    <div class="container-fluid">
        <div class="row fondo-servicio lujos_accesorios">
            <div class="col-xs-12">
                &nbsp;
            </div>
        </div>
        <div class="row fondo-azul">				
            <div class="col-xs-12" style="padding:35px;">
                <h1 class="text-blanco text-titulo">
                    Lujos y accesorios
                </h1>
                <br />
                <div class="container">
                    <div class="col-md-3">
                        <ul style="list-style: none;">
                            <li><span style="color: #ffffff;"><i class="fa fa-check-circle"></i> Punteras de exosto</span></li>
                            <li><span style="color: #ffffff;"><i class="fa fa-check-circle"></i> Pitos y cornetas.</span></li>
                            <li><span style="color: #ffffff;"><i class="fa fa-check-circle"></i> Exploradoras.</span></li>
                            <li><span style="color: #ffffff;"><i class="fa fa-check-circle"></i> Kit de carretera.</span></li>
                            <li><span style="color: #ffffff;"><i class="fa fa-check-circle"></i> Ambientadores.</span></li>
                        </ul>
                    </div>
                    <div class="col-md-4">
                        <ul style="list-style: none;">
                            <li><span style="color: #ffffff;"><i class="fa fa-check-circle"></i> Consolas universales</span></li>
                            <li><span style="color: #ffffff;"><i class="fa fa-check-circle"></i> Tapetes en alfombra y caucho.</span></li>
                            <li><span style="color: #ffffff;"><i class="fa fa-check-circle"></i> Pedales</span></li>
                            <li><span style="color: #ffffff;"><i class="fa fa-check-circle"></i> Plumillas</span></li>
                            <li><span style="color: #ffffff;"><i class="fa fa-check-circle"></i> Bombilleria y leds de alta intensidad en general.</span></li>
                        </ul>
                    </div>
                    <div class="col-md-4">
                        <ul style="list-style: none;">
                            <li><span style="color: #ffffff;"><i class="fa fa-check-circle"></i> Siliconas.</span></li>
                            <li><span style="color: #ffffff;"><i class="fa fa-check-circle"></i> Shampoo.</span></li>
                            <li><span style="color: #ffffff;"><i class="fa fa-check-circle"></i> Ceras.</span></li>
                            <li><span style="color: #ffffff;"><i class="fa fa-check-circle"></i> Desmanchadoras.</span></li>
                            <li><span style="color: #ffffff;"><i class="fa fa-check-circle"></i> Forros timon.</span></li>
                            <li><span style="color: #ffffff;"><i class="fa fa-check-circle"></i> Asegurada de espejos retrovisores y biseleria</span></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">				
            <div class="col-xs-12 col-md-7">
                <div class="row">
                    <div class="col-xs-6 col-sm-4">
                        <a href="{{ asset('/img/servicios/autoforros_lujos_1.jpg') }}" data-toggle="lightbox" data-gallery="multiimages" data-title="">
                            <img class="img-responsive" style="width:100%;margin-top:10px;" src="{{ asset('/img/thumbs/autoforros_lujos_1.jpg') }}" />
                        </a>
                    </div>
                    <div class="col-xs-6 col-sm-4">
                        <a href="{{ asset('/img/servicios/autoforros_lujos_2.jpg') }}" data-toggle="lightbox" data-gallery="multiimages" data-title="">
                            <img class="img-responsive" style="width:100%;margin-top:10px;" src="{{ asset('/img/thumbs/autoforros_lujos_2.jpg') }}" />
                        </a>
                    </div>
                    <div class="col-xs-6 col-sm-4">
                        <a href="{{ asset('/img/servicios/autoforros_lujos_3.jpg') }}" data-toggle="lightbox" data-gallery="multiimages" data-title="">
                            <img class="img-responsive" style="width:100%;margin-top:10px;" src="{{ asset('/img/thumbs/autoforros_lujos_3.jpg') }}" />
                        </a>
                    </div>
                    <div class="col-xs-6 col-sm-4">
                        <a href="{{ asset('/img/servicios/autoforros_lujos_4.jpg') }}" data-toggle="lightbox" data-gallery="multiimages" data-title="">
                            <img class="img-responsive" style="width:100%;margin-top:10px;" src="{{ asset('/img/thumbs/autoforros_lujos_4.jpg') }}" />
                        </a>
                    </div>
                    <div class="col-xs-6 col-sm-4">
                        <a href="{{ asset('/img/servicios/autoforros_lujos_5.jpg') }}" data-toggle="lightbox" data-gallery="multiimages" data-title="">
                            <img class="img-responsive" style="width:100%;margin-top:10px;" src="{{ asset('/img/thumbs/autoforros_lujos_5.jpg') }}" />
                        </a>
                    </div>
                    <div class="col-xs-6 col-sm-4">
                        <a href="{{ asset('/img/servicios/autoforros_lujos_6.jpg') }}" data-toggle="lightbox" data-gallery="multiimages" data-title="">
                            <img class="img-responsive" style="width:100%;margin-top:10px;" src="{{ asset('/img/thumbs/autoforros_lujos_6.jpg') }}" />
                        </a>
                    </div>
                    <div class="col-xs-6 col-sm-4">
                        <a href="{{ asset('/img/servicios/autoforros_lujos_7.jpg') }}" data-toggle="lightbox" data-gallery="multiimages" data-title="">
                            <img class="img-responsive" style="width:100%;margin-top:10px;" src="{{ asset('/img/thumbs/autoforros_lujos_7.jpg') }}" />
                        </a>
                    </div>
                    <div class="col-xs-6 col-sm-4">
                        <a href="{{ asset('/img/servicios/autoforros_lujos_8.jpg') }}" data-toggle="lightbox" data-gallery="multiimages" data-title="">
                            <img class="img-responsive" style="width:100%;margin-top:10px;" src="{{ asset('/img/thumbs/autoforros_lujos_8.jpg') }}" />
                        </a>
                    </div>
                    <div class="col-xs-6 col-sm-4">
                        <a href="{{ asset('/img/servicios/autoforros_lujos_9.jpg') }}" data-toggle="lightbox" data-gallery="multiimages" data-title="">
                            <img class="img-responsive" style="width:100%;margin-top:10px;" src="{{ asset('/img/thumbs/autoforros_lujos_9.jpg') }}" />
                        </a>
                    </div>
                    <div class="col-xs-6 col-sm-4">
                        <a href="{{ asset('/img/servicios/autoforros_lujos_12.jpg') }}" data-toggle="lightbox" data-gallery="multiimages" data-title="">
                            <img class="img-responsive" style="width:100%;margin-top:10px;" src="{{ asset('/img/thumbs/autoforros_lujos_12.jpg') }}" />
                        </a>
                    </div>
                </div>
            </div>
            @include('form_cotizacion')
        </div>
    </div>		
</div><!-- .content-area -->

@stop