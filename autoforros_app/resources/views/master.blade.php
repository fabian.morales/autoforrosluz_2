<!doctype html>
<html lang="{{ app()->getLocale() }}" class="no-js">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Autoforros Luz</title>

        <!-- Fonts -->
        <link href="{{ asset('/css/bootstrap.css') }}" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.css" />
        <link rel="stylesheet" href="{{ asset('/css/font-awesome.min.css') }}" />
        <link rel="stylesheet" href="{{ asset('/js/lightslider/css/lightslider.min.css') }}" />
        <link href="{{ asset('/css/style.css') }}" rel="stylesheet" type="text/css" />

        <link rel="shortcut icon" type="image/ico" href=" {{ asset('favicon.ico') }}" />

        @section('js_header')
        @show

        <!-- Google Analytics Code -->
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        
            ga('create', 'UA-54371236-5', 'auto');
            ga('require', 'displayfeatures');
            ga('send', 'pageview');
        </script>
        <!--Start of Zopim Live Chat Script-->
        <script type="text/javascript">
            window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
            d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
            _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
            $.src='//v2.zopim.com/?3pNaFHPJiggBjM0v70goAjOGcmF8smz4';z.t=+new Date;$.
            type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
        </script>
        <!--End of Zopim Live Chat Script-->
    </head>
    <body>
        <div id="page" class="site">
            <div class="site-inner" style="padding-top:30px;">
                <a class="skip-link screen-reader-text" href="#content">Skip</a>
                <div class="container">
                    <div class="col-xs-12 col-md-4">
                        <img src="{{ asset('img/logo.png') }}" />
                    </div>
                    <div class="col-xs-12 col-md-5">
                        <br />
                        <p class="text-right text-azul"><b><i class="fa fa-phone" aria-hidden="true"></i> (57 - 2) 449 03 39 <br><i class="fa fa-whatsapp" aria-hidden="true"></i> Cel. (57) 312 394 53 55</b></p>
                    </div>
                    <div class="col-xs-12 col-md-3">
                        <a href="{{ url('/contacto') }}"><img src="{{ asset('/img/solicitar-informacion.png') }}" class="hidden-xs hidden-sm img-responsive"></a>                
                    </div>
                </div>
        
                <header id="masthead" class="site-header" role="banner">
                    <div class="site-header-main">                
                        <button id="menu-toggle" class="menu-toggle">Menu</button>
                        <div id="site-header-menu" class="site-header-menu">                        
                            <nav id="site-navigation" class="main-navigation" role="navigation" aria-label="main-navegacion">
                                <ul id="menu-main-navigation" class="primary-menu">
                                    <li><a href="{{ url('/') }}">Inicio</a></li>
                                    <li><a href="{{ url('/nosotros') }}">Nosotros</a></li>
                                    <li>
                                        <a>Servicios</a>
                                        <ul>
                                            <li><a href="{{ url('/forros') }}">Forros</a></li>
                                            <li><a href="{{ url('/tapiceria') }}">Tapiceria</a></li>
                                            <li><a href="{{ url('/aire-acondicionado') }}">Aire acondicionado</a></li>
                                            <li><a href="{{ url('/alarma-y-bloqueo') }}">Alarma y bloqueo</a></li>
                                            <li><a href="{{ url('/polarizados') }}">Polarizados</a></li>
                                            <li><a href="{{ url('/lujos-y-accesorios') }}">Lujos y accesorios</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="{{ url('/contacto') }}">Contacto</a></li>
                                    <li><a href="https://accounts.zoho.com">Acceso a Mail</a></li>
                                </ul>
                            </nav><!-- .main-navigation -->
                        </div><!-- .site-header-menu -->
                    </div><!-- .site-header-main -->
                </header><!-- .site-header -->
            
                <div id="content" class="site-content">
                    @yield('content')
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div id="contenedor-marcas">
                                <ul id="slider-marcas">
                                    <li><img src="{{ asset('/img/marcas-chevrolet.png') }}" /></li>
                                    <li><img src="{{ asset('/img/marcas-hyundai.png') }}" /></li>
                                    <li><img src="{{ asset('/img/marcas-kia.png') }}" /></li>
                                    <li><img src="{{ asset('/img/marcas-mazda.png') }}" /></li>
                                </ul>
                            </div>
                        </div>      
                    </div>		       
                </div>
                <br />
                <div class="container">
                    <div class="row">       
                        <div class="col-xs-12">
                            <div class="fondo-azul" style="padding-bottom:40px;">
                                <div class="row">    
                                    <h2 class="text-center text-blanco text-titulo" style="margin: 30px 10px 50px 10px;">CONTACTO</h2>
                                    <div class="col-xs-6 col-md-3 block-footer">        
                                        <img src="{{ asset('/img/footer-direccion.png') }}" class="img-responsive center-block" /><br />
                                        <h5 class="text-center text-blanco">Calle 52 Cra 1D Esquina.<br>Centro Comercial Carrera. <br />Local 121</h5>
                                    </div>        
                                    <div class="col-xs-6 col-md-3 block-footer">
                                        <img src="{{ asset('/img/footer-tel.png') }}" class="img-responsive center-block" /><br />
                                        <h5 class="text-center text-blanco">(57 - 2) 449 03 39</h5>
                                    </div>
                                    <div class="col-xs-6 col-md-3 block-footer">
                                        <img src=" {{ asset('/img/footer-whatsapp.png') }}" class="img-responsive center-block" /><br />
                                        <h5 class="text-center text-blanco">(57) 312 394 53 55</h5>
                                    </div>
                                    <div class="col-xs-6 col-md-3 block-footer">
                                        <img src="{{ asset('/img/footer-email.png') }}" class="img-responsive center-block" /><br />        
                                        <h5 class="text-center text-blanco">info@autoforrosluz.com</h5>
                                    </div>  
                                </div>
                            </div>
                        </div>        
                    </div>       
                </div>
                <br />
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="fondo-negro" style="padding:20px 0;">
                                <p class="text-center text-blanco" style="margin: 0"> © {{ date('Y') }} Autoforros Luz.<br>Todos los derechos reservados.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script>jQuery.noConflict();</script>

        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.js"></script>
        <script src="{{ asset('js/lightslider/js/lightslider.min.js') }}"></script>
        
        @section('js_body')
        @show

        <script type="text/javascript">
            (function($){
                $(document).ready(function() {
                    $(document).on('click', '*[data-toggle="lightbox"]', function(event) {
                        event.preventDefault();
                        $(this).ekkoLightbox();
                    });

                    $('#slider-marcas').lightSlider({
                        item: 4,
                        enableTouch:true,
                        enableDrag:true,
                        freeMove:true,
                        loop: true,
                        pager: false
                    });
                });
            })(jQuery);
        </script>
    </body>
</html>
