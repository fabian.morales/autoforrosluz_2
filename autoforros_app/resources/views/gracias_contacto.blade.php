@extends('master')

@section('content')
<!-- Google Code for Solicitud De Contacto - Autoforros Luz Conversion Page -->
<script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 926712630;
    var google_conversion_language = "en";
    var google_conversion_format = "3";
    var google_conversion_color = "ffffff";
    var google_conversion_label = "_DL-CMnlhmoQtobyuQM";
    var google_remarketing_only = false;
    /* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
    <div style="display:inline;">
        <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/926712630/?label=_DL-CMnlhmoQtobyuQM&guid=ON&script=0"/>
    </div>
</noscript>

<div id="primary" class="content-area" style="width:100%;">		
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12" style="padding:40px;">
                <h1 style="text-align: center;" class="text-titulo">Gracias</h1>
                <br />
                <h3 style="text-align: center;">En breve nos pondremos en contacto con usted para brindar respuesta a sus inquietudes o comentarios.</h3>
                <h3 style="text-align: center;">Recuerde que también puede comunicarse con nosotros a través de Teléfono fijo, WhatsApp o eMail.</h3>
                <p class="text-center text-azul"><b><i class="fa fa-phone"></i> (57 - 2) 449 03 39
                <i class="fa fa-whatsapp"></i> Cel. (57) 314 742 13 23
                <i class="fa fa-envelope-o"></i> info@autoforrosluz.com</b></p>
            </div>
        </div>
        <br />
    </div>		
</div><!-- .content-area -->

@stop