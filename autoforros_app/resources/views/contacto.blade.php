@extends('master')

@section('content')

<div id="primary" class="content-area" style="width:100%;">
    <div class="container-fluid">
        <div class="row">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3982.4953002859834!2d-76.50645568574761!3d3.4719433520528735!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8e30a63296eca933%3A0xd30a82d9f7a1b3a1!2sAutoforros+Luz!5e0!3m2!1ses!2sco!4v1471537518841" style="height:400px;border:0;width:100%;" allowfullscreen></iframe>
        </div>
        <div class="row">
            @include('form_contacto')
        </div>
    </div>
</div>

@stop