<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('/forros', function () {
    return view('forros');
});

Route::get('/aire-acondicionado', function () {
    return view('aire_acondicionado');
});

Route::get('/alarma-y-bloqueo', function () {
    return view('alarma_bloqueo');
});

Route::get('/lujos-y-accesorios', function () {
    return view('lujos_accesorios');
});

Route::get('/polarizados', function () {
    return view('polarizados');
});

Route::get('/tapiceria', function () {
    return view('tapiceria');
});

Route::get('/nosotros', function () {
    return view('nosotros');
});

Route::get('/contacto', function () {
    return view('contacto');
});

Route::get('/gracias-por-contactarnos', function() {
    return view('gracias_contacto');
});

Route::get('/gracias-por-su-interes', function() {
    return view('gracias_interes');
});